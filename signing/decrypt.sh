#!/bin/sh

ENCRYPT_KEY=$1

if [ "$ENCRYPT_KEY" != "" ]; then
  # Decrypt Release key
  openssl aes-256-cbc -d -in signing/release.aes -out signing/release.jks -k $ENCRYPT_KEY
else
  echo "ENCRYPT_KEY is empty"
fi

